<html><head><meta charset="UTF-8"> </head><body> 
<h1>APACHE</h1>
<p>Kursun son gunlerinde doruk fişek hocamızın anlattığı web konusunu sizlere aktarmaya çalışacağım.</p>
<p>Doruk hoca konuyu slayt üzerinden anlattı. Sonrasında bazı görevler verip bunları yapmamızı istedi.  Kendi notlarım ve hocanın anlattıklarından aklımda kalanları anlatmayı planlıyorum. Herhangi bir konuda yanlış görürseniz lütfen bildirmeyi unutmayın.</p>
<ul>
<li>Derste bir adet centos minimal sanal makine istenmektedir.</li>
<li>Anlatımda ki hataları değiştirmek için: <a href="https://gitlab.com/rection/lyk18-SistemYonetimi-2-duzey.git">Gitlab</a></li>
<li>Ders de kullanılan slayt için: <a href="http://topluluk.ozguryazilim.com.tr/wp-content/sunumlar/sistemyonetimi-sunumlar/apache_web_sunucusu.html#1">Slayt</a></li>
</ul>
<h2>Web Mimarisi</h2>
<p><img alt="Alt text" src="apache-dorukfisek/eklenti/index.gif" />  </p>
<p>Fotoğrafda ki istemciler web tarayıcılarıdır. Web tarayıcıları, internette yayınlanmış bir sayfaya ulaşmak için bir arayüzdür. Bunlara örnek verir isek firefoxi chrome, safari gibi araçlardır. Tarayıcıdan herhangi adresi arattığınız zaman, ağlar aracılığıyla adresin bulunduğu sunucuya gidilmektedir. Sunucudan sayfa istenmesi için sunucu da web sunucusu olmalıdır.  </p>
<p>Web sunucusu, istenen sayfanın olup olmadığını kontrol edip istenen sayfayı vermektir. Eğer sayfa bulunamadıysa hata kodu dönmektedir. Eğer sayfa bulunursa, yapılmasını istenen farklı bir işlem varsa (kayıt, veri işleme...) uygulama sunucusuna yönlendirilir. Uygulama sunucusu, gelen isteğin içinde bulunan işlemleri yapması için yönlendiren ve kontrol eden mekanizmadır. Uygulama sunucusuna örnek verirsek zend(php), J2EE(java) gibi araçlardır. Eğer sistemde veritabanı kullanılmış ise uygulama sunucusu veritabanına istek gönderip işlenmesini sağlar. Veritabanını ile uygulama sunucusu arasında haberleşme http protokolü ile yapılır.  </p>
<p>HTTP: Hyper Text Transfer Protocol, 80. port kullanılmaktadır.<br />
HTTPS: HTTP Secure (Güvenli), 443. port kullanılmaktadır.<br />
URL: Bir web sunucusunun bulunduğu konumun, insanlar tarafından okunan kısmıdır.  </p>
<p>Netcraft'ın verdiği rakama göre (Eylül 2018) web sunucusu payları:  </p>
<p><img alt="Alt Img" src="apache-dorukfisek/eklenti/websunucu.png" />    </p>
<p>StatCounter'ın verdiği rakama göre (Ağustos 2018) istemci payları:  </p>
<p><img alt="Alt Img" src="apache-dorukfisek/eklenti/sonbrowser.png" />  </p>
<h3>Apache Tarihçesi</h3>
<p>1991 - HTTP'nin 0.9 duyuruldu.<br />
1994 - NCSA HTTPd geliştirilmesi durdu.<br />
1995 - NCSA temelli "A Patchy Server" çıktı.<br />
1996 - Zamanla ismi Apache olup dünyanın en popüler sunucusu oldu.<br />
1999 - Apache Software Foundation kuruldu.<br />
2018 - 350+ Apache projesiyle Apache dünyanın en popüler sunucusudur.  </p>
<h3>Apache Kurulumu</h3>
<p>Apache'nin kurulumu oldukça basittir. Centos 7 dağıtımı üzerinden anlatılmıştır. Debian dağıtımlarında komutlar ve konfigürasyon dosyalarının yeri değişmektedir.  </p>
<blockquote>
<p><code># yum install httpd</code>  </p>
</blockquote>
<p>httpd paketini indirip, yüklemektedir.    </p>
<blockquote>
<p><code># service httpd start</code>  </p>
</blockquote>
<p>Yükleme sonrasında servisi çalıştırıyoruz.  </p>
<blockquote>
<p><code># chkconfig httpd on</code>  </p>
</blockquote>
<p>Her açılışta tekrardan başlatmamak için bu komutu uyguluyoruz.  </p>
<blockquote>
<p><code># sudo service firewalld start</code>  </p>
</blockquote>
<p>Dışarıdan bağlantıya izin verilmesi için firewalld servisini başlatıyoruz.</p>
<blockquote>
<p><code># sudo firewall-cmd --permanent --add-port=80/tcp</code>  </p>
</blockquote>
<p>Firewall üzerinden 80.portu tcp protokolü için açıyoruz.  </p>
<p>Ayar dosyaları ise şunlardır;  </p>
<blockquote>
<p>/etc/httpd/conf/httpd.conf<br />
/etc/httpd/conf.d/*.conf<br />
/etc/sysconfig/httpd  </p>
</blockquote>
<h3>Çalışma Modelleri</h3>
<p>MPM (Multi-Processing Modules): Web sunucusunun temel işlevlerini değiştirmek için kullanılır. Bu apache'nin modüler dizaynı sayesinde olmaktadır. Bütün http işlemlerinde kullanılır. Bu aşamaları kullanmak için üç tane seçim aşaması vardır. Bunlar Prefork, Worker ve Event'tır.</p>
<blockquote>
<p><strong>Prefork (öntanımlı):</strong> Apache ilk calıştırıldığında alt süreçlerle başlamaktadır. Bunlar öntanımlı olarak gelen istekleri karşılamak için çalışan servislerdir. Prefork bunun kontrol edilmesini sağlar.</p>
<p><strong>Worker (Threaded):</strong> Bir bağlantı olduğu zaman her alt işlemler yeni kuyruklar oluşturur. Prefork'tan farkı burada bağlantı olduğu zaman yeni bir alt işlem oluşturulur.</p>
<p><strong>Event (Threaded - Apache 2.4+):</strong> Diğer işlemlere göre daha fazla isteği aynı anda sunulmasına izin verir. Burada ilk isteği tamamladıktan sonra istemci bağlantıyı açık tutabilir. Bu sayede aynı soketi kullanarak daha fazla istek gönderebilir ve bağlantıya aşırı yüklenmesini azaltır.    </p>
</blockquote>
<p>Nginx varsayılan olarak Worker(Threaded) çalışan bir servistir. Apache varsayılan olarak prefork çalışmaktadır. Her iki web sunucusunu da anlatılan aşamalardan birine geçirebiliriz.   </p>
<h3>Moduler Yapısı:</h3>
<p>Birçok ek işlev httpd core'unun üzerine eklenir. Httpd core, moduler yapıyı yönetmeyi sağlamaktadır. Apache'nin en iyi özelliği modül biçiminde olmasıdır. Her defasında modüllerin okunması gerektiğinden dolayı, ne kadar az modül eklenirse o kadar performanslı çalışır. Modüller hem dinamik yüklenebilir, hem de statik olarak gömülebilmektedir. Statik olması avantaj olarak görülmektedir fakat herhangi bir değişiklik yapıldığı zaman, tekrardan derlenmesi gerekmektedir. Bu da sistemin yavaşlamasına sebep olmaktadır.</p>
<p>Statik derlenmiş modülleri görmek için <strong><code># httpd -l</code></strong> komutu kullanılır.</p>
<h4>Modüllerin Eklenmesi/Çıkarılması:</h4>
<p>/etc/httpd/conf/httpd.conf dosyasının içinde 51. satırda <code>LoadModule auth_basic_module modules/mod_auth_basic.so</code> bulunmaktadır. 'modules', /etc/httpd/Modules dizinini belirtir. Eklemek istediğiniz modülü modules dizini içine koyulmalıdır.</p>
<p>Apache'nin kendi modüllerinden bazıları şunlardır:  </p>
<ul>
<li>mod_dav: HTTP protokolü ile uzak sunucu da kaynakların oluşturulması, taşınması ve silinmesi gibi işlemlerin yapılmasını sağlar.  </li>
<li>mod_deflate: Sunucu tarafında sıkıştırılma işlemi yapılmasını sağlamaktadır.  </li>
<li>mod_ldap: Arka tarafta çalışan LDAP servisinin performansını geliştirmek için kullanılan modüldür.  </li>
</ul>
<p>Apache'nin yaygın kullanılan ek modüllerden bazıları şunlardır:</p>
<ul>
<li>mod_pagespeed: Otomatik olarak web optimizasyonu sağlar.</li>
<li>mod_security: Kendi için firewall sistemi vardır. Sql injection gibi güvenlik açıkları için de kullanılabilir.</li>
<li>mod_wsgi: Python dilinde yazılan uygulamanın ayarları ve sunumunda kullanılır.  </li>
</ul>
<h2>Temel Ayarlar:</h2>
<p>/etc/httpd/conf/httpd.conf dosyasının 30. satırda <strong>ServerRoot</strong> ayarı bulunmaktadır. Yapılandırma dosyalarının nerede bulunduğunu belirtir.  </p>
<p>/etc/httpd/conf/httpd.conf dosyasında 109. satırda <strong>DocumentRoot</strong> ayarı bulunmaktadır. Sunulacak olan dosyanın yerine belirtir. Genellikle /var/www/html'dir.  </p>
<p>/etc/httpd/conf/httpd.conf dosyasında 88. satırda <strong>ServerName</strong> ayarı bulunmaktadır. Özel hostname ve port numarasının girildiği yerdir. Hostname olma zorunluluğu yoktur. Domain name servisine kayıtlı olan www.exapmle.com gibi adres de olabilir.</p>
<p>/etc/httpd/conf/httpd.conf dosyasında 40. satırda <strong>Listen</strong> ayarı bulunmaktadır. Hangi ip adresi ve port üzerinden sunum yapacağını belirtir. ipv6 desteklemektedir.  </p>
<p>Virtualhost tanımlarken kullanılan <strong>ServerAlias</strong> ise birden fazla domain name tanımlamaya yarar.  </p>
<h2>Dizinlere Özgü Ayarlar:</h2>
<p>Öncelikle tek dizinlerin kullanımından bahsedeceğim.  </p>
<blockquote>
<p><code>&lt;Directory /&gt;</code><br />
<code>Options FollowSymLinks</code>  <br />
<code>AllowOverride None</code>  <br />
<code>&lt;/Directory&gt;</code>  </p>
</blockquote>
<p><strong>Options FollowSymLinks:</strong> Varsayılan olarak gelmektedir. Sunucunun dizin içinde sembolik linkleri takip etmesini ifade eder. Daha fazla bilgi için <a href="https://httpd.apache.org/docs/2.4/mod/core.html#Options%20Directive">tıklayınız</a>.</p>
<p><strong>AllowOverride None:</strong> Dizin sisteminde dosyaları okuyabilmesini reddetmesini ifade etmektedir. <strong><em>.htaccess</em></strong> dosyaları için önemlidir. Uygulanabilecek diğer metodlar kimlik doğrulaması(AuthConfig), Dosya bilgilendirmesi(FileInfo)... Ayrıntılara bakmak için <a href="https://httpd.apache.org/docs/2.4/mod/core.html#allowoverride">tıklayınız</a>.</p>
<p>Çoklu dizinler de kullanılması için şu şekilde ayarlanmalıdır.</p>
<blockquote>
<p><code>&lt;DirectoryMatch /[a-d].*&gt;</code><br />
<code>Options -FollowSymLinks</code><br />
<code>AllowOverride All</code><br />
<code>&lt;/DirectoryMatch&gt;</code>  </p>
</blockquote>
<p>DirectoryMatch'da tanımlanan '/[a-d].*' kısımında  anlatılmak istenen a ve d arasında ki harfler ve sonrasında gelen dizin eşlemeleri için bu kuralları uygula demektir.<br />
<strong>Options -FollowSymLinks</strong> kısımında bulunan '-' bu özelliğin kapatılmasını ifade etmektedir. Varsayılan olarak + gelmektedir.  <br />
<strong>AllowOverride All</strong> dosyaları okumasına izin verdiğini belirtmektedir.</p>
<p>Bunlara benzer şekilde <strong><em>dosya, url, proxy ve virtualhostlara</em></strong> özgü ayarlar bulunmaktadır. Bazı önemli ayarlar vardır.</p>
<p><strong>Order allow,deny:</strong> <strong><em>İzin verme kurallarından</em></strong> önce <strong><em>Reddet kurallarınının</em></strong> işlenenceğini belirtmektedir. Eğer client <strong><em>İzin kurallarıyla</em></strong> eşleşme yapmazsa veya eşleşen <strong><em>Reddet kuralı</em></strong> var ise o zaman client erişimi reddeder.  </p>
<p><strong>Order deny,allow:</strong> <strong><em>Reddet kurallarından</em></strong> önce <strong><em>İzin verme kurallarının</em></strong> işleneceğini belirtmektedir. Eğer client <strong><em>Reddet kuralıyla</em></strong> eşleşme yapmazsa veya eşleşen <strong><em>İzin kuralı</em></strong> var ise client erişime izin verir.  </p>
<p>Bu satırın altında Deny(Reddet) ve Allow(İzin verme) satırları yazılması zorunluluktur. Daha ayrıntılı bilgi için <a href="http://www.maxi-pedia.com/Order+allow+deny">tıklayınız</a>.  </p>
<h2>DirectoryIndex</h2>
<p>Dizinlere erişimlerin ayarlanması ve  hata kodlarının buna göre ayarlanmasını da sağlar. Tanımlı dosya eşleşmez ise Indexes özelliği inclenir. Indexes yetkisi varsa dosya listesi gösterilir. Indexes yetkisi yoksa "403 Forbidden" döndürür.  </p>
<p><strong>ServerTokens:</strong> Sunucunun döndüğü cevabın başında sunucunun başında taşınan bilgilerdir. Apachenin ve İşletim sisteminin versiyonuna göre verilen bilgileri göstermektedir. Daha fazla bilgi için <a href="https://httpd.apache.org/docs/2.4/mod/core.html#servertokens">tıklayınız</a>.  </p>
<p><strong>StartServers 10:</strong> Varsayılan olarak 10 tane süreç aç ve isteğin gelmesini bekle demektir. Daha fazla bilgi için <a href="https://httpd.apache.org/docs/2.4/mod/mpm_common.html#startservers">tıklayınız</a>.  </p>
<p><strong>MinSpareServers 5:</strong> Boştaki alt süreçlerinin asgari sayısını belirler. Boştaki süreç, o an bir isteğe hizmet sunmayan süreçtir. Eğer MinSpareServers sayıda süreçten daha az boşta süreç varsa ana süreç sayıyı tamamlamak için yeni çocuk süreçler oluşturacaktır. Daha fazla bilgi edinmek için <a href="https://httpd.apache.org/docs/2.4/tr/mod/prefork.html">tıklayınız</a>.  </p>
<p><strong>MaxSpareServers 20:</strong> Boştaki alt süreçlerinin azami sayısını belirler. Boştaki süreç, o an bir isteğe hizmet sunmayan süreçtir. Eğer MaxSpareServers sayıda süreçten daha fazla boşta süreç varsa ana süreç bu fazlalıkları öldürecektir. Daha fazla bilgi edinmek için <a href="https://httpd.apache.org/docs/2.4/tr/mod/prefork.html">tıklayınız</a>.  </p>
<p><strong>MaxRequestsPerChild 4000:</strong> Kaç işlem sonrasında işlemin öldüreceğini belirtir. Daha fazla bilgi için <a href="https://httpd.apache.org/docs/2.4/mod/mpm_common.html#MaxRequestsPerChild">tıklayınız</a>.  </p>
<p><strong>MaxClients 150:</strong> Ram miktarına göre maksimum clients ihtiyacına göre ayarlamaya yarar. Daha fazla bilgi için <a href="https://2bits.com/articles/tuning-the-apache-maxclients-parameter.html">tıklayınız</a>.  </p>
<p><strong>ServerLimit 256:</strong> Apache işlemlerinin maksimum harcayabileceği ram miktarını belirler. MaxRequestWorkers ile orantılı bir şekilde ayarlanmalıdır. Daha fazla bilgi edinmek için <a href="https://httpd.apache.org/docs/current/en/mod/mpm_common.html">tıklayınız</a>.  </p>
<p><strong>KeepAlive On:</strong> Arka arkaya istek gelmesine izin vermektedir. Daha fazla bilgi için <a href="https://httpd.apache.org/docs/2.4/mod/core.html#KeepAlive">tıklayınız</a>.  </p>
<p><strong>KeepAliveTimeout 15:</strong> Sunucu kalıcı bir bağlantıyı kapatmadan önce, sonraki isteği kaç saniye bekleyeceğini belirtir. Kullanıcıların hızını bakılıp ayarlanması gerekmektedir. Daha fazla bilgi edinmek için <a href="https://httpd.apache.org/docs/2.4/tr/mod/core.html">tıklayınız</a>.  </p>
<p><strong>MaxKeepAliveRequests 4000:</strong> Bağlantı başına izin verilecek istek sayısını sınırlar. Sunucu başarımını yüksek tutmak için yüksel bir değer olmalıdır. Daha fazla bilgi için <a href="https://httpd.apache.org/docs/2.4/mod/core.html#maxkeepaliverequests">tıklayınzı</a>.  </p>
<h2>.htaccess</h2>
<p>.htaccesss sunulacak olan verilerin bulunduğu dizine özgü ayarların yapılmasını sağlayan bir araçtır. Ana sunucu yapılandırma dosyasına erişilemediğinde veya değistirilemediğinde .htaccess dosyası kullanılmalıdır. Ayrıca eski siteden yeni siteye veya eski sayfalardan yeni sayfalara yönlendirmek için kullanılır.</p>
<ul>
<li>httpd.conf içinde AccessFileName ayarı etkinleştirilmelidir.<blockquote>
<pre><code> AccessFileName .htaccess
</code></pre>
</blockquote>
</li>
<li>httpd.conf'ta yapılan ayarlar dizin bazlı değiştirir.  </li>
<li>Apache öncelikle her dizinde .htaccess dosyasını arar.  </li>
<li>O dizin ve altındaki dizinler için bu ayarlar geçerli olur.  </li>
</ul>
<h4>Avantajlar:</h4>
<ul>
<li>httpd.conf'u düzenleme yetkisi gerektirmez.    </li>
<li>Ayarlar anında etkili olur.  </li>
<li>Dizinin taşınmasında ayarlar da taşınır.  </li>
</ul>
<h4>Dezavantajlar:</h4>
<ul>
<li>Performansı düşürür. Bir belge istendiğinde her dizinin kök dizinine kadar daha yukarı bakmalıdır ve her dizinin değiştirilmesi aşamasında tekrar en başta ki .htaccess dosyasını okumaktadır. Bu da yavaşlamasına sebep olmaktadır.  </li>
<li>Güvenliği azaltır. Dosyalarındaki yönergelerin yanlış yapılandırması, dizindeki ve alt dizinlerin içindeki belgeler de sorunlara neden olabilir.</li>
</ul>
<h4>Alias</h4>
<p>Dosyaların sunulacağı dizinin farklı bir yerde saklanmasını mümkün kılar. Url yolu ile dizin yolunun eşlenmesini sağlar.</p>
<h4>Redirect</h4>
<p>Eski bir url'i yönlendirmek için kullanılır. Https'e zorlamak için kullanılan bir yöntemdir. Redirect'de regex kullanılamaz iken <em>RedirectMatch</em> ile kullanılabilmektedir.  </p>
<blockquote>
<p><code>Redirect 301 /eski_yeri.html http://www.ozguryazilim.com.tr/yeni_yeri.php</code><br />
<code>RedirectMatch ^/images/(.*)$ http://images.ozguryazilim.com.tr/$1</code></p>
</blockquote>
<p><em>Rewrite konusunu hocamız slaytta yazılanları anlattı. Zaten slayt yeterince bu konu hakkında açıklayıcıdır.</em>  </p>
<h2>SSL ile Şifreleme:</h2>
<p>Açılımı Secure Sockets Layer'dir. Amacı bir websitesi ile ziyaretçinin taraycısı arasında güvenli bağlantı oluşturmasını sağlamaktadır. Diğer deyişle karşı tarafın ulaşmak istediğimiz yer olduğunu doğrulamaktır. SSL domain name içindir. Sunucu için değildir. Daha fazla bilgi edinmek için <a href="http://www.networksolutions.com/education/what-is-an-ssl-certificate/">tıklayınız</a>.</p>
<p>SSL kurulum aşamasından bahsetmiyoruz çünkü dersin son kısımda verilen görevlerden birisidir.</p>
<p>Eğer dosya upload edilecekse öncelikle /tmp/ dizinin altına indirilmektedir. Sonrasında aktarılmaktadır. Bunların tanımlanması php sunulmasında kullanılır.</p>
<h2>Proxy:</h2>
<p>Basit bir tanımla internet erişimi sırasında kullanılar ara sunucudur. Sadece tarayıcı üzerinden ayarlanabilmektedir. Bir bağlantıda öncelikle isteğiniz proxy sunucusuna sonrasında internete açılmaktadır. Kullanılmasının amacı yasaklı sitelere girilebilmesidir ama vpn gibi değildir. Proxy sunucusu ile aranızda ki bağlantı şifreli değildir ve proxy sunucusu https desteklemeyebilir. İki çeşidi vardır.</p>
<p><img alt="proxy" src="apache-dorukfisek/eklenti/ReverseProxy.jpg" /></p>
<h4>Forward Proxy(Yönlendirilmiş proxy):</h4>
<p>Yukarıda anlatılan olan konu forward proxy'dir.</p>
<h4>Reverse Proxy(Ters Proxy):</h4>
<p>Forward proxy'nin tersidir. Client tarafından gelen istekleri karşılayıp arkada ki konumlandırılmış sunucuya yönlendirir.</p>
<p>İkisinin farkı: Forward proxy client tarafının kimliğini saklarken, Reverse proxy sunucunun kimliğini saklamasını sağlamaktadır.</p>
<p>Ters proxy ile sunucu tarafınta ssl yükünü kaldırabiliyoruz. Bu sayede sunucunun yükü azalmaktadır. Debug ve capture etmek için sunucu tarafında http kullanılmalıdır. </p>
<h2>Apache Logları</h2>
<p>Varsayılan olarak log kayıtları /var/log/httpd/ altında tutulmaktadır. İsteğe göre client ismi ile tutulması mümkündür. Logların gün sonunda sıkıştırılıp imzalanması gerekmektedir. Loglar herhangi bir yasal durum karşısında sorun oluşmaması için 2 yıl saklanması gerekmektedir. Logların tutulmaması karşısında suçlu kayıt tutması gereken kişi olmaktadır. Ayrıntılı bilgi için <a href="http://www.mevzuat.gov.tr/MevzuatMetin/1.5.5651.pdf">tıklayınız</a>.</p>
<p>Logların analizinin yapılması için bazı araçlardan bahsedildi. Bunlardan en fazla piwik kullanılmaktadır.</p>
<h3><strong>Doruk Hoca'nın verdiği ödevler:</strong></h3>
<p>1-)SSL<br />
2-)Reverse Proxy -----&gt; hürriyet.com.tr'ye yönlendirmesi istenmektedir.<br />
3-)Virtualhost -------&gt;test1.linux.gen.tr<br />
               | <br />
               -------&gt;test2.linux.gen.tr<br />
4-)Parolalı alan oluşturulması istenmektedir.<br />
5-)PHP-fpm kurulumu -----&gt; reverse proxy ----&gt; phpinfo()<br />
6-)IP kurulumu</p>
<p>Sonraki günlerde ödevlerin sonuçları açıklanacaktır. <strong><em>Yapım Aşamasında</em></strong></p></body>
</html>
